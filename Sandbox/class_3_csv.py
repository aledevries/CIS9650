# -*- coding: utf-8 -*-
"""
Created on Sat Feb 18 14:37:30 2017

@author: VriesA
"""

import csv

with open("channels.txt") as file:
    rows = file.read().split('\n')
    records = []
    for row in rows:
        #row = row.rstrip('\n')
        columns = (row.split(','))
        record = {columns[0] : [ e for e in columns[1:]]}
        records.append(record)