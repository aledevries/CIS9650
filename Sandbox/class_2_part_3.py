# -*- coding: utf-8 -*-
"""
Created on Sat Feb 11 14:46:15 2017

@author: vriesa
"""

grades = []

f = open('data.txt')
lines = f.readlines()

for line in lines:
    grades.append(int(line))
f.close()

try:
    average = sum(grades)/len(grades)
 
    print ("Points average:",average)

    if average >= 90:
        print ("Letter grade: A")
    elif average >= 80:
        print ("Letter grade: B")
    elif average >= 70:
        print ("Letter grade: C")
    else:
        print ("Letter grade: F")

except (ZeroDivisionError):
    print ("Your forgot to enter a grade!")