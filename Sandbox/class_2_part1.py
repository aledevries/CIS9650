# -*- coding: utf-8 -*-
"""
Created on Sat Feb 11 12:35:36 2017

@author: vriesa
"""

grade = 0
total = 0
i = 0
 
while grade != -1:
    total += grade
    grade = int(input("Enter grade %d: " % (i + 1) ))
    i += 1
i -= 1

average = total/(i)
print ("Points average:",average)

if average >= 90:
    print ("Letter grade: A")
elif average >= 80:
    print ("Letter grade: B")
elif average >= 70:
    print ("Letter grade: C")
else:
    print ("Letter grade: F")