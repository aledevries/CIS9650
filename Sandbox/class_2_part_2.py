# -*- coding: utf-8 -*-
"""
Created on Sat Feb 11 12:35:36 2017

@author: vriesa
"""

grades = []
grade = 0
total = 0
i = 0
 
while grade != -1:
    grade = int(input("Enter grade %d: " % (i + 1) ))
    if grade != -1:
        grades.append(grade)
        i += 1

try:
    average = sum(grades)/len(grades)
 
    print ("Points average:",average)

    if average >= 90:
        print ("Letter grade: A")
    elif average >= 80:
        print ("Letter grade: B")
    elif average >= 70:
        print ("Letter grade: C")
    else:
        print ("Letter grade: F")

except (ZeroDivisionError):
    print ("Your forgot to enter a grade!")