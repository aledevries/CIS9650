# -*- coding: utf-8 -*-
"""
Created on Sat Feb 11 14:46:15 2017

@author: vriesa
"""
def get_let_grade(average):
    if average >= 90:
        return "A"
    elif average >= 80:
        return "B"
    elif average >= 70:
        return "C"
    else:
        return "F"

grades = { }

f = open('data.txt')
lines = f.readlines()

for line in lines:
    record = line.split()
    name = record[0]
    grade = int(record[1])
    if name in grades:
        grades[name].append(grade)
    else: grades[name] = [grade]
    
f.close()
print (grades)

let_grades = {}

for name in grades:
    let_grades[name] = get_let_grade(sum(grades[name])/len(grades[name]))
    
print (let_grades)

