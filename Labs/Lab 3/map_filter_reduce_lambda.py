# -*- coding: utf-8 -*-
"""
Created on Sat Mar 18 13:23:44 2017

@author: VriesA
"""

# Based on Lab 2: NYC Leading causes of death

#Get death count
def DeathCount(data, r, g, y1, y2):
    # Following three operations could be rolled into one
    ## Filter data for year range
    fList = filter(lambda x: x.year > y1 and x.year <= 2, data)
    ## Filter data for gender
    fList = filter(lambda x: x.gender == g, fList)
    ## Filter data for race
    fList = filter(lambda x: x.race == r, fList)
    
    # Following operation maps the objects in the list to a specific attribute
    #  of each object
    mList = list(map(lambda x: x.dcount, fList))    # Since filters and maps are 'lazy lists',
                                                    #   it's only here  - when list() is called -
                                                    #   that the actual filtering and mapping happens,
                                                    #   in a chain reaction.
    return sum(mList)


# What is the leading cause of death amongst females?
females = list(filter(lambda x: x.gender = 'F', data))
maxFemale = max(females, key=lambda x: x.dcount)    # Returns the object with the 
                                                    #   maximum death count (not the 
                                                    #   death count itself)
print (maxFemale.cause)