# -*- coding: utf-8 -*-
"""
CIS 9650 Lab 3
Created on Sat Mar 18 12:25:38 2017
@author: VriesA
"""

import csv

# Define classes
class Agency():
    def __init__(self, name):
        self.name = name

class Platform():
    def __init__(self, name):
        self.name = name

class Presence():
    def __init__(self, agency, platform, url, date_sampled, count):
        self.agency = agency
        self.platform = platform
        self.url = url
        self.date_sampled = date_sampled
        self.count = count

# Get data
f = open("social_media.csv")
data = [row for row in csv.DictReader(f)]
data = [row for row in data if (
        row['Url'][:7] == 'http://' and
        row['Likes/Followers/Visits/Downloads'] and
        row['Agency'] != 'Total'
        )]

# Populate data structes
agency_names = []
platform_names = []
agencies = []
platforms = []

for record in data:
    agency_name = record['Agency']
    if agency_name not in agency_names:
        agency_names.append(agency_name)
        agencies.append(Agency(agency_name))
    platform_name = record['Platform']
    if platform_name not in platform_names:
        platform_names.append(platform_name)
        platforms.append(Platform(platform_name))
   
presences = []
     
for agency in agencies:
    for platform in platforms:
        for record in data:
            if record['Agency'] == agency.name and record['Platform'] == platform.name:
                presences.append(Presence(agency,
                                          platform,
                                          record['Url'],
                                          record['Date Sampled'],
                                          int(record['Likes/Followers/Visits/Downloads'])
                                          )
                                )


#Feature 1:
def most_pop_presence():
    return max(presences, key = lambda x: x.count)

most_pop = most_pop_presence()

print ("Most popular presence: {}, at {}, with {} Likes/Followers/Visits/Downloads".format(
        most_pop.agency.name, most_pop.url, most_pop.count))

#Feature 2:
def print_agency_counts():
    for agency in agencies:
        lst = filter(lambda x: x.agency == agency, presences)
        counts = list(map(lambda x: x.count, lst))
        agency.count = len(counts)
        print (agency.name, agency.count)

print ("List of agencies with their counts:")        
print_agency_counts()

#Feature 3.a:
def most_pop_platform():
    for platform in platforms:
        lst = filter(lambda x: x.platform == platform, presences)
        counts = list(map(lambda x: x.count, lst))
        platform.popularity = sum(counts)
    return max(platforms, key = lambda x: x.popularity)

pop_platform = most_pop_platform()
print ("\nMost popular platform: {}".format(pop_platform.name))

#Feature 3.b:
def avg_popularity(platform):
    lst = filter(lambda x: x.platform == platform, presences)
    counts = list(map(lambda x: x.count, lst))
    return sum(counts)/len(counts)

avg_platform_pop = avg_popularity(pop_platform)

print ("Average popularity: {}".format(avg_platform_pop))
