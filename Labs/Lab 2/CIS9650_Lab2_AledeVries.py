# -*- coding: utf-8 -*-
"""
CIS 9650 - Lab 2
Created on Sat Mar 11 13:37:14 2017
@author: Ale de Vries
"""

import csv
import matplotlib.pylab as plt

# Get data
## import csv file
f = open('nyc_deaths.csv')
## Read data into list, using DictReader to get field names
rows = [row for row in csv.DictReader(f)]
## Replace periods (which denote empty values) in 'Deaths' with zero, conver to 
##  int
for row in rows:
    if not row['Deaths'].isdigit():
        row['Deaths'] = 0
    else: row['Deaths'] = int(row['Deaths'])

# Feature #1: report the total deaths in NYC
death_count = 0
for row in rows:
    death_count += (row['Deaths'])
print ("Total number of deaths since start: {}".format(death_count))



# Feature #2: Ask user to enter race, gender and year.

## Define function
def DeathCount(race = None,
               gender = None,
               year_range = None):
    '''Returns the death count for a given race and gender and year range'''
    if (race and gender and year_range):
        death_count = 0
        for row in rows:
            if (row['Race Ethnicity'] == race and
                    row['Sex'] == gender and
                    year_range[0] <= int(row['Year']) <= year_range[1]):
                death_count += row['Deaths']
        return death_count

### Get user input
#race = input("Please enter the race: ")
#gender = input("Please enter the gender: ")
#start_year = int(input("Please enter start year: "))
#end_year = int(input("Please enter end year: "))
#
### Show results
#print ("Number of deaths for this demographic and time span: {}".format(
#        DeathCount(race, gender, (start_year, end_year))))

# Feature #3: Generate a graph 
##
unique_years = []
for row in rows:
    year = row['Year']
    if year not in unique_years:
        unique_years.append(year)
unique_years = sorted(unique_years)

deaths_by_year = {}
for year in unique_years:
    tot_for_year = 0
    for row in rows:
        if row['Year'] == year:
            tot_for_year += row['Deaths']
        deaths_by_year[year] = tot_for_year

print ("Trend of deaths over year:")
fig = plt.figure()
ax = plt.subplot(111)
x_values = [int(e) for e in list(deaths_by_year.keys())]
ax.bar(x_values,list(deaths_by_year.values()))
fig.show()