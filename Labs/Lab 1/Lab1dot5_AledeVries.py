# -*- coding: utf-8 -*-
"""
CIS 9650 - Lab 1.5
@author: Ale de Vries
Created on Sat Feb 25 12:37:11 2017
"""

from numpy import average

# Reading file and using list comprehension to populate data array, stripping
#   off newline chars and splitting data points into pairs in the course.
with open('streaming.txt') as file:
    data_points = [e.rstrip('\n').split(' ') for e in file.readlines()]
# There is no need to convert song numbers from strings to integers, since
#   we are not doing arithmetic operations on them.

# Create data dictionary that groups songs by user
user_dict = {}
for e in data_points:
    if e[0] in user_dict:
        user_dict[e[0]].append(e[1])
    else:
        user_dict[e[0]] = [e[1]]

# Create data dictionary that groups users by songs
song_dict = {}
for e in data_points:
    if e[1] in song_dict:
        song_dict[e[1]].append(e[0])
    else:
        song_dict[e[1]] = [e[0]]
        
# Determine and print most popular song
pop_song = []
popularity = 0
for key in song_dict:
    if len(song_dict[key]) > popularity:
        pop_song = [key]
        popularity = len(song_dict[key])
    elif len(song_dict[key]) == popularity:
        pop_song.append(key)
        
print("Most popular song(s): {} with {} occurences.".format((', ').join(pop_song), popularity))

# Determine and print most active user
act_user = []
activity = 0
for key in user_dict:
    if len(user_dict[key]) > activity:
        act_user = [key]
        activity = len(user_dict[key])
    elif len(user_dict[key]) == activity:
        act_user.append(key)
print("Most active user(s): {} with {} songs.".format((', ').join(act_user), activity))

# Determine and print average length of songlist
average_list_length = average([len(e) for e in user_dict.values()])
print("Average length of songlist is {}".format(average_list_length))


# Recommend song function.
def recommend_song(user, user_dict = user_dict):
    '''Recommends a song for this user based on the playlist of other users.''' 
    # Popping the given user from the user dictionary, so that I can operate
    #   on the others more easily
    user_songs = user_dict.pop(user) 
    # Initiating variables to keep track of best match
    max_score = 0
    user_match = ''
    # Outer loop iterates through all other users.
    for other_user in user_dict:
        score = 0
        # Inner loop iterates over all songs for that other user and determines
        #   how many of them match the songs of the given user
        for song in user_dict[other_user]:
            if song in user_songs:
                score += 1
        # Check if the score of this user match is greater than the current max
        #   score; if so, upgrade the max_score
        if score > max_score:
            max_score = score
            user_match = other_user
    # Determine and return list of matching songs, recommendations, user match
    match_list = user_dict[user_match]
    recommendations = ([e for e in match_list if e not in user_songs])
    return (user_match, max_score, recommendations)

user = input("Enter user (default = 'F'): ")
if not user:
    user = 'F'
matching_user, matching_songs, recommendations = recommend_song(user)

print ('''
User %s is similar to user %s; they have %s song(s) in common.
We recommend song(s) %s to F.
''' % (user, matching_user, matching_songs, ', '.join(recommendations)))
