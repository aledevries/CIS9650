# -*- coding: utf-8 -*-
"""
CIS 9650 - Lab 1
@author: Ale de Vries
Created on Sat Feb 25 12:37:11 2017
"""

# Import standard deviation and average from numpy
from numpy import std, average

# Reading file and using filtered list comprehension to populate data array
with open('cbpss.txt') as file:
    ratings = [int(e) for e in file.readlines() if (0 < int(e) < 5)]

# Print high, low, average
print ("Highest rating: {}".format(max(ratings)))
print ("Lowest rating: {}".format(min(ratings)))
print ("Average rating: {}".format(int(average(ratings))))

# Print frequency counts, again using filtered list comprehension
print ("# of 1 ratings: {}".format(len([e for e in ratings if e == 1])))
print ("# of 2 ratings: {}".format(len([e for e in ratings if e == 2])))
print ("# of 3 ratings: {}".format(len([e for e in ratings if e == 3])))
print ("# of 4 ratings: {}".format(len([e for e in ratings if e == 4])))

# Print standard deviation with 3 digits precision
print ("Standard deviation: {:.3f}".format(std(ratings)))