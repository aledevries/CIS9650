# -*- coding: utf-8 -*-
"""
CIS 9650 - Lab 4
Created on Sat Mar 25 21:10:27 2017
@author: Ale de Vries
"""
# Import dependencies
import csv
import matplotlib.pylab as plt


# Define School class
class School():
    '''Represents a school with its SAT statistics.'''
    
    def __init__(self,
                 name,
                 num_takers,
                 reading_score,
                 math_score,
                 writing_score):
        '''Constructor.'''
        self.name = name
        self.num_takers = int(num_takers)
        self.reading_score = int(reading_score)
        self.math_score = int(math_score)
        self.writing_score = int(writing_score)
        
    def __str__(self):
        # Prettier for console output.
        return "School '{}'".format(self.name)
    
    def __repr__(self):
        # Prettier for console output.
        return "School '{}' at address {}".format(self.name, hex(id(self)))
    
    
    # Defining the following methods as properties so that they behave like
    #   attributes - e.g. you can call .math_mind rather than .math_mind()
   
    @property
    def math_mind(self):
        '''Returns the ratio between average math score and next-highest 
        average score for this school.'''
        return self.math_score / max(self.reading_score, self.writing_score)
    
    @property
    # Defining this method as a property so that it appears as an attribute - 
    #   i.e. you can call it with .math_mind rather than math_mind()
    def tot_score(self):
        '''Returns the ratio between average math score and next-highest 
        average score for this school.'''
        return self.math_score + self.reading_score + self.writing_score
    
    def GetMathMindScore():
        # Implementing this because the assignment said "Make sure that your
        #   class has a method GetMathMindScore()
        '''Returns the ratio between average math score and next-highest 
            average score for this school. Same as School.math_mind'''
        return self.math_mind
            
# Read and load data
reader = csv.reader(open('SAT_Results.csv'))
next(reader)
# Some schools in the input file have a non-numerical value for their scores, 
#  so skipping those. Also, ignoring the first column.
rows = [row[1:] for row in reader if (
        row[2].isdigit() and row[3].isdigit() and row[4].isdigit() and row[5].isdigit())]
schools = []
for row in rows:
    schools.append(School(row[0],row[1],row[2],row[3],row[4]))
    
# Define features

## Feature 1
def max_and_min_schools(schools = schools):
    return {
            'max' : max(schools, key = lambda x: x.tot_score),
            'min' : min(schools, key = lambda x: x.tot_score)
            }

## Feature 2
def plot_scores_vs_school_size(schools = schools,
                               bucket_limits = [100, 200, 500, 1000]):
    data_points = {}
    index = 0
    lower_limit, upper_limit = 0, 0
    # Iterate through the first (n-1) buckets
    for bucket_limit in bucket_limits:
        lower_limit, upper_limit = upper_limit, bucket_limit
        f_schools = list(filter(
                lambda x: x.num_takers > lower_limit and
                x.num_takers <= upper_limit,
                schools))
        avg_score = (
                sum(list(map(lambda x: x.tot_score, f_schools))) /
                    len (f_schools))
        label = str(lower_limit) + '-' + str(upper_limit)
        data_points[index] = (label, avg_score)
        index += 1
    # Don't forget to populate the open-ended top bucket!
    f_schools = list(filter(lambda x: x.num_takers > upper_limit, schools))
    avg_score = (sum(list(map(lambda x: x.tot_score, f_schools))) /
                 len(f_schools))
    label = '>' + str(upper_limit)
    data_points[index] = (label, avg_score)
    
     # Initiate bar chart (more appropriate for aggregations like this)
    plt.bar(range (len(data_points)),
        [data_points[data_point][1] for data_point in data_points],
         )
    # Add bin labels
    plt.xticks(range(len(data_points)),
               [data_points[data_point][0] for data_point in data_points],
               size = 'small',
               rotation='vertical'
    )
    # Add axis labels
    plt.ylabel('Total average SAT score')
    plt.xlabel('Number of SAT takers per school')
    # Show this sucker.
    plt.show()
    return None

## Feature 3
def top_5_math_minds(schools = schools):
    return list(sorted(schools, key = lambda x: x.math_mind, reverse = True))[:5]

# Demonstrate features

## Feature 1
print('=== FEATURE 1 ===')
maxmin = max_and_min_schools()
highest, lowest = maxmin['max'], maxmin['min'], 
print ('These are the schools with the highest and lowest average total SAT scores:')
print ("Highest: {} with {} average total SAT".format(highest.name, highest.tot_score))
print ("Lowest: {} with {} average total SAT".format(lowest.name, lowest.tot_score))
print('\n')

## Feature 2
print('=== FEATURE 2 ===')
plot_scores_vs_school_size()
print('\n')

## Feature 3
print('=== FEATURE 3 ===')
top_5 = top_5_math_minds()
idx = 1
print('These are the five schools with the top MathMind scores:')
for school in top_5:
    print ('{} - {} with MathMind score {:.3f}'.format(
            idx, school.name, school.math_mind))
    idx += 1    