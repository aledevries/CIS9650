# -*- coding: utf-8 -*-
"""
CIS 9650 - Assignment 6
Created on Thu Feb 23 23:56:55 2017
@author: Ale de Vries
"""

# Initialize data set
data_points = []

with open('input.txt') as in_file:          # This automatically closes the
    for line in in_file:                    #   input file again.
        line = line.rstrip('\n')            # Python 3.x doesn't strip newline.
        key, value = tuple(line.split(' ')) # Extract key/value pair from line.
        data_points.append(float(value))    # Store value in data set.

# Note that I'm using 'new-style' string formatting to write the output file.
#   This is because new-style formatting automatically handles data types and 
#   float precision in a nice way.
with open('output.txt', mode = 'w') as out_file:
    out_file.write(
'''*******************************************
               TICKET REPORT
*******************************************

There are {} tickets in the database. 

Maximum Ticket price is ${}.
Minimum Ticket price is ${}.
Average Ticket Price is ${}.

Thank you for using our ticket system!

*******************************************
'''.format(len(data_points),
            max(data_points),
            min(data_points),
            sum(data_points)/len(data_points))
)