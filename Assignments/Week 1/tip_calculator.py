# -*- coding: utf-8 -*-
"""
Created on Mon Feb  6 23:48:22 2017

@author: VriesA
"""

meal = 44.50
tax = 0.0675
tip = 0.15

meal = meal + meal * tax
total = meal + meal * tip

print("%.2f" % total)