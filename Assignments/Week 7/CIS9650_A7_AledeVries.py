# -*- coding: utf-8 -*-
"""
CIS 9650 Assignment week 7
Created on Wed Mar 22 22:13:34 2017
@author: Ale de Vries
"""

import csv
import matplotlib.pylab as plt

# define classes
## NOTE: I probably went a little overboard here. I first only had a Flight and
##  an Airport class, but since Feature 3 asks for on-time record plotted
##  against distance, it became apparent to me that the notion of a route was
##  also relevant. Since I was in full OOP mode, I decided to therefore just
##  create a Route class as well.

class Route():
    '''Represents a route between two airports. Each route can have multiple
        flights.'''
    def __init__(self, origin, destination, dist):
        '''Constructor'''
        self.origin = origin
        self.destination = destination
        self.distance = int(dist)
        
    def __str__(self):
        # Prettier for console output.
        return "Route ({} to {}) over {} miles".format(
                self.origin,
                self.destination,
                self.distance)
        
    def __repr__(self):
        # Prettier for console output.
        return "Route ({}, {}, {}) at address {}".format(
                self.origin, self.destination, self.distance, hex(id(self)))

    def __eq__(self, other):
        # Allows comparison of route on value (i.e. using '==' )
        return (self.origin == other.origin and
                self.destination == other.destination and
                self.distance == other.distance)


class Flight():
    '''Represents a flight occurence between two airports'''
    
    def __init__(self, route, delay):
        '''Constructor'''
        self.route = route
        self.delay = int(delay)
        
    def __str__(self):
        # Prettier for console output.
        return "Flight ({} with delay {})".format(self.route, self.delay)
        
    def __repr__(self):
        # Prettier for console output.
        return "Flight ({}, {}) at address {}".format(
                self.route, self.delay, hex(id(self)))
        

class Airport():
    '''Represents an airport.'''

    def __init__(self, code):
        self.code = code

    def __str__(self):
        # Prettier for console output.
        return "Airport '{}'".format(self.code)
        
    def __repr__(self):
        # Prettier for console output.
        return "Airport '{}' at address {}".format(self.code, hex(id(self)))
    
    def __eq__(self, other):
        # Allows comparison of airport on value (i.e. using '==' )
        return self.code == other.code

    # Feature 1: determine % of delayed flights for given departure airport
    def on_time_record(self):
        '''Returns the fraction of on-time departures at this airport.'''
        fList = list(filter(lambda x: x.route.origin == self, flights))
        # There shouldn't be any zero division with this data set, but it 
        #   never hurts to check.
        try:
            return len(list(filter(lambda x: x.delay == 0, fList))) / len(fList)
        except ZeroDivisionError:
            return None
        
# Get data
## Open file
f = open('airline_delays.csv')
## Read data into list, using DictReader to get field names
rows = [row for row in csv.DictReader(f)]
## Close file
f.close()

# Load rows into data structures, skipping incomplete rows
flights = []
# Using 'set' ensures unique values
airports = set([])
routes = set([])
for row in rows:
    # Ignore arrival delays
    row.pop('ARR_DELAY_NEW')
    # Only create a Flight object if no column is empty
    if '' not in row.values():
        flights.append(Flight(
                Route(Airport(row['ORIGIN']), Airport(row['DEST']), row['DISTANCE']),
                row['DEP_DELAY_NEW'], 
                ))
        airports.add(row['ORIGIN'])
        airports.add(row['DEST'])
        routes.add((row['ORIGIN'], row['DEST'], row['DISTANCE']))
# Turn set of airport names into list of unique Airports.
airports = [Airport(e) for e in airports]
routes = [Route(Airport(e[0]),Airport(e[1]),e[2]) for e in routes]


# Feature 2: determine airports with best and worst on-time records
def best_and_worst_dep_rec():
    '''Returns the airports with the best and worst on-time records as a
        dictionary with keys 'best' and 'worst'.'''
    return {'best' : max(airports, key = lambda x: x.on_time_record()),
            'worst' : min(airports, key = lambda x: x.on_time_record())}
    
# Feature 3: look for correlation between on-time record and flight distance
## NOTE: the assignment is to group flights - i.e. use binning - which means 
##  that this is not really a correlation exercise. I'm therefore using a bar
##  chart, which is more appropriate for representing aggregations like that.

def plot_otr_vs_distance(bin_size = 200):
    '''Plots a histogram with the average delay per distance bin, given a 
    certain bin size.'''
    # Determine minimum and maximum flight distances, and based on that the 
    #   number of bins and first/last bins
    min_distance = min(routes, key = lambda x: x.distance).distance
    max_distance = max(routes, key = lambda x: x.distance).distance
    bin_start_idx = int(min_distance/bin_size)
    bin_end_idx = int(max_distance/bin_size)
    # Populate bins
    bins = {}
    # Iterate through bins
    for i in range (bin_start_idx, bin_end_idx):
        lower_bound = i * bin_size
        upper_bound = (i + 1) * bin_size
        # Determine which routes fall in this bin
        routes_in_bin = list(filter(
            lambda x: x.distance > lower_bound and x.distance <= upper_bound,
            routes))
        # Calculate the average on-time record for each origin airport in
        #   these routes
        avg_on_time_record = (
                sum(list(map(
                        lambda x: x.origin.on_time_record(),
                        routes_in_bin))) / (len(routes_in_bin)))
        # Add this bin to the list of bins
        bins[i] = {
                'label' : str(lower_bound + 1) + '-' + str(upper_bound),
                'avg_otr' : avg_on_time_record }
    # Initiate bar chart
    plt.bar(range (len(bins)),
        [bins[binn]['avg_otr'] for binn in bins],
         )
    # Add bin labels
    plt.xticks(range(len(bins)),
               [bins[binn]['label'] for binn in bins],
               size = 'small',
               rotation='vertical'
    )
    # Add axis labels
    plt.ylabel('On-time record at departure airport')
    plt.xlabel('Route distance')
    # Show this sucker.
    plt.show()
    return True

# Main event loop
while True:
    print(
'''\nWelcome to AirDelay 2017
=========================
1 - Delays by airport
2 - Best and Worst Airports
3 - On time/distance correlation
0 - Exit''')
    choice = input("Choice: ")
    if choice == '0':
        break
    elif choice == '1':
        airport_code = input('Enter 3-letter airport code: ')
        for airport in airports:
            if airport == Airport(airport_code):
                print ('On time {:.1%} of the time'.format(airport.on_time_record()))
                print ('Delayed {:.1%} of the time'.format(1-airport.on_time_record()))
                break
    elif choice == '2':
        best = best_and_worst_dep_rec()['best']
        worst = best_and_worst_dep_rec()['worst']
        print ('Best on-time record: {} with {:.1%}'.format(best.code, best.on_time_record()))
        print ('Worst on-time record: {} with {:.1%}'.format(worst.code, worst.on_time_record()))
    elif choice == '3':
        plot_otr_vs_distance()
    else:
        print ('Invalid choice.')