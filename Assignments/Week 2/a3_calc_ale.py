# -*- coding: utf-8 -*-
"""
CIS 9650 Assignment 3 - calculator module, incl. tests
Created on Wed Feb 15 22:10:08 2017
@author: Ale de Vries
"""

def calc_premium(client_age,num_accidents):
    """ Calculates and returns the premium based on the client's age and past
        accident history. If number of past accidents is greater than 4,
        returns False, indicating that insurance should be refused.
    """
    # set base premium
    premium = 50
    
    # return False if more than 4 accidents
    if num_accidents > 4:
        return False
    
    # add charge for past accidents, if relevant
    if num_accidents == 4:
        premium += 240
    elif num_accidents == 3:
        premium += 180
    elif num_accidents == 2:
        premium += 80
    
    # add charge for age, if relevant
    if client_age < 25:
        premium += 100
    elif client_age < 35:
        premium += 20
    
    # return resulting premium
    return premium


# tests functions for pytest. Run from shell as "pytest a3_calc_ale.py"
def test_age_24_acc_0():
    """Tests premium calculation for age = 24, number of accidents = 0"""
    assert calc_premium (24,0) == 150

def test_age_24_acc_3():
    """Tests premium calculation for age = 24, number of accidents = 3"""
    assert calc_premium (24,3) == 330

def test_age_26_acc_0():
    """Tests premium calculation for age = 26, number of accidents = 0"""
    assert calc_premium (26,0) == 70

def test_age_50_acc_2():
    """Tests premium calculation for age = 50, number of accidents = 2"""
    assert calc_premium (50,2) == 130
