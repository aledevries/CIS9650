# -*- coding: utf-8 -*-
"""
CIS 9650 Assignment 4 - YAA module
Created on Wed Feb 15 23:26:40 2017
@author: Ale de Vries
"""

def show_payouts(num_months):
    """Calculates and shows the payouts for all months that the user won, 
        returns the last payout."""
    if num_months == 0:
        return False
    payout = 1
    print ("Payout in month 1: ", payout)
    for i in range (2,num_months + 1):  
        if i % 2 == 0:
            payout *= 2
        else:
            payout *= 3
        print ("Payout in month %d: " % i, payout)
    return payout

# test cases for pytest. Run from shell as "pytest a4_yaa_ale.py"
def test_show_payouts_1():
    assert show_payouts(1) == 1

def test_show_payouts_2():
    assert show_payouts(2) == 2

def test_show_payouts_3():
    assert show_payouts(6) == 72

def test_show_payouts_10():
    assert show_payouts(10) == 2592
                       
def test_show_payouts_12():
    assert show_payouts(12) == 15552