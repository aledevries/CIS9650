# -*- coding: utf-8 -*-
"""
CIS 9650 Assignment 4 - main module
Created on Wed Feb 15 23:58:21 2017
@author: VriesA
"""

from a4_yaa_ale import show_payouts

# main program

# I do love me some ASCII art.
print ("""\a        
                                   .''.       
       .''.      .        *''*    :_\/_:     . 
      :_\/_:   _\(/_  .:.*_\/_*   : /\ :  .'.:.'.
  .''.: /\ :   ./)\   ':'* /\ * :  '..'.  -=:o:=-
 :_\/_:'.:::.    ' *''*    * '.\'/.' _\(/_'.':'.'
 : /\ : :::::     *_\/_*     -= o =-  /)\    '  *
  '..'  ':::'     * /\ *     .'/.\'.   '
      *            *..*         :
        *
        *
        
             !!!CONGRATULATIONS!!!
                 You have won.
        Want to know your monthly payouts?
    Enter the number of months you won below!
        """)

show_payouts(int(input("Number of months: ")))