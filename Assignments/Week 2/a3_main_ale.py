# -*- coding: utf-8 -*-
"""
CIS 9650 Assignment 3 - main program
Created on Wed Feb 15 22:45:37 2017
@author: Ale de Vries
"""
from a3_calc_ale import calc_premium

# main program flow
print ("=== CAR INSURANCE PREMIUM CALCULATOR ===")

# get age and accident history
age = int(input("Enter client age: "))
num_acc = int(input("Enter the number of car accidents client has had: "))

# calculate premium
premium = calc_premium(age, num_acc)

# Show result
if premium:
    print ("Your monthly premium is $%d" % premium)
else:
    print ("You have had too many accidents in the past. We can't insure you.")