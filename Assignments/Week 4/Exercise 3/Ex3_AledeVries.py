# -*- coding: utf-8 -*-
"""
Created on Wed Mar  1 21:58:54 2017

@author: vriesa
"""
from statistics import mean

# Define Truck class 
class Truck:
    '''Represents a truck in our company'''
    
    # Constructor
    def __init__(self, truck_id):
        '''Initiates a truck'''
        self.id = truck_id
        self.earnings = {}
        self.expenses = {}
        self.latest_read_errors = []
    
    def earned(self, month):
        try:
            return sum(self.earnings[month])
        except KeyError:
            return False
        
    def spent(self, month):
        try:
            return sum(self.expenses[month])
        except KeyError:
            return False
        
    def profit(self, month):
        return self.earned(month) - self.spent(month)
    
    def avg_earn(self, month):
        return mean(self.earnings[month])
        
    def read_data(self, month):
        '''Reads the earnings and expenses data for this truck for a given 
            month.'''
        try:
            with open(month + '.csv') as f:
                lines = f.readlines()
                lines = [e.rstrip('\n') for e in lines]
                records = [e.split(',') for e in lines]
                self.earnings[month] = [int(e[2]) for e in records if (
                        e[0] == self.id
                        and e[1] == 'earned')]
                self.expenses[month] = [int(e[2]) for e in records if (
                        e[0] == self.id
                        and e[1] == 'spent')]
                self.latest_read_errors = [e for e in records if (
                        e[0] != self.id or (
                                e[1] != 'earned' and e[1] != 'spent')
                        )]
                return True
        except FileNotFoundError:
            return False

# Main program flow      

## Initiate trucks
truck_a, truck_b = Truck('A'), Truck('B')

## Input loop
while True:
    month = input("Enter the name of the reporting month in 'mmmyy' format: ")
    if month == 'exit':
        break;
    elif not (truck_a.read_data(month) and truck_b.read_data(month)):
        print ("Please check your input.")
    else:
        common_errors = (
                [e for e in truck_a.latest_read_errors if e in truck_b.latest_read_errors] +
        [e for e in truck_b.latest_read_errors if e in truck_a.latest_read_errors]
        )
        print ("Data import errors: {}. Please check data file for this month.".format(common_errors))
        print ("On truck A, we spent ${}.".format(truck_a.spent(month)))
        print ("On truck A, we earned ${}.".format(truck_a.earned(month)))
        print ("On truck A, we had a profit of ${}.".format(truck_a.profit(month)))
        print ("On truck A, we had average earnings of ${:.2f}.".format(truck_a.avg_earn(month)))
        print ()
        print ("On truck B, we spent ${}.".format(truck_b.spent(month)))
        print ("On truck B, we earned ${}.".format(truck_b.earned(month)))
        print ("On truck B, we had a profit of ${}.".format(truck_b.profit(month)))
        print ("On truck B, we had average earnings of ${:.2f}.".format(truck_b.avg_earn(month)))
        print ()
        print ("In total, we spent ${}.".format(truck_a.spent(month) + truck_b.spent(month)))
        print ("In total, we earned ${}.".format(truck_a.earned(month) + truck_b.earned(month)))
        print ("In total, we had a profit of ${}.".format(truck_a.profit(month) + truck_b.profit(month)))
