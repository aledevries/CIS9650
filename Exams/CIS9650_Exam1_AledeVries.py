# -*- coding: utf-8 -*-
"""
CIS 9650 - Exam 1 - Ale de Vries
"""

import matplotlib.pyplot as plt


# Open and read data file
data = []

while not data:
    # Ask user for file name.
    file_name = input("Enter the name of the file to open: ")
    # Try loading the file.
    try:
        with open(file_name) as f:
            data = f.readlines()
    except:
        # Assumes, for simplicity's sake, that the exception thrown is an I/O
        #   error due to wrong filename.
        print ("File does not exist. Please try again.")
        
# Convert data points from str to int, while filtering out outliers
clean_data = [ int(e) for e in data if ( -200 <= int(e) <= 200) ]
outliers = [ int(e) for e in data if int(e) not in clean_data]

# Inform user of outliers
for e in outliers:
    print ("Ignoring out of range temperature: {:.1f}".format(e))
    
# Search for spikes
print ("\nSearching for spikes...")
spikes = []
# Iterate from second to second-last values in list (first and last datapoints
#   can by definition not be spikes)
for i in range (1,len(clean_data)-1):
    # Calculate average of previous and next data point
    avg_prev_next = (clean_data[i-1]+clean_data[i+1])/2
    if clean_data[i] > 1.5 * avg_prev_next:
        # Store the spike with previous/next data point as a tuple
        spikes.append((clean_data[i-1], clean_data[i], clean_data[i+1]))
        
# Inform user of spikes
for spike in spikes:
    print (
    "Detected spike. Previous: {:.1f}, spike: {:.1f}, next: {:.1f}".format(
        spike[0], spike[1], spike[2]))
    
# Calculate average, and how many values are higher or lower
average = sum(clean_data)/len(clean_data)
num_lower = len([e for e in clean_data if e < average])
num_higher = len([e for e in clean_data if e > average])
print ("\n{} values are lower than the average.".format(num_lower))
print ("{} values are higher than the average.".format(num_higher))

# plot temperatures
plt.plot(clean_data)
plt.show()

# Determine the two minimum and maximum values
#   Sort data ascendingly
sorted_clean_data =  sorted(clean_data)
min_1 = sorted_clean_data[0]
min_2 = sorted_clean_data[1]
max_1 = sorted_clean_data[-1]
max_2 = sorted_clean_data[-2]

# Show mins, maxes and average
print ("Min. temp. 1: {:.1f}".format(min_1))
print ("Min. temp. 2: {:.1f}".format(min_2))
print ("Max. temp. 1: {:.1f}".format(max_1))
print ("Max. temp. 2: {:.1f}".format(max_2))
print ("Avg. temp.: {:.1f}".format(average))