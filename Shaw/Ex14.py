# -*- coding: utf-8 -*-
"""
Created on Sun Feb 12 23:08:10 2017

@author: vriesa
"""
from sys import argv

script, user_name= argv
prompt = ">"

print ("Hi %s, I'm the %s script" % (user_name, script))
print ("I'd like to ask you a few questions.")
print ("Do you like me %s?" % user_name)
likes = input(prompt)

print ("Where do you live?")
lives = input(prompt)

print ("What kind of computer do you have?")
computer = input(prompt)

print ("")
print ("""Alright, so you said %s about liking me.
You live in %s. Not sure where that is.
And you have a %s computer. Nice.
""" % (likes, lives, computer))