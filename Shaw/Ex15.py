# -*- coding: utf-8 -*-
"""
Created on Tue Feb 14 23:43:59 2017

@author: VriesA
"""
from sys import argv

script, filename = argv

txt = open(filename)
print ("Here's your file %s" % filename)

print (txt.read())
print ("Type the file name again:")
file_again = input("> ")

txt_again = open(file_again)

print (txt_again.read())