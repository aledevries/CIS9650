print ("I will now count my chickens:") # Prints textual line

print ("Hens", 25 + 30 / 6) # Calculates and prints number of hens, using addition and division
print ("Roosters", 100 - 25 * 3 % 4) # Calculates and prints number of roosters, using subtraction and modulus (remainder)

print ("Now I will count the eggs:") # Prints textual line

print (3 + 2 + 1 - 5 + 4 % 2 - 1 / 4 + 6) # Calculates and prints number of eggs, using addition, subtraction, modulus and division

print ("Is it true that 3 + 2 < 5 - 7?") # Prints textual line

print (3 + 2 < 5 - 7) # Prints result of Boolean expression

print ("What is 3 + 2?", 3 + 2) # Prints result of simple addition
print ("What is 5 - 7?", 5 - 7) # Prints result of simple subtraction

print ("Oh, that's why it's False.") # Prints textual line

print ("How about some more.") # Prints textual line

print ("Is it greater?", 5 > -2) # Prints result of Boolean expression
print ("Is it greater or equal?", 5 >= -2) # Prints result of Boolean expression
print ("Is it less or equal?", 5 <= -2) # Prints result of Boolean expression