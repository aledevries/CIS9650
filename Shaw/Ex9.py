#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 12 14:15:31 2017

@author: aledevries
"""

days = "Mon Tue Wed Thu Fri Sat Sun"
months = "\nJan\nFeb\nMar\nApr\nMay\nJun\nJul\nAug\nSep\nOct\nNov\nDec"

print ("Here are the days:", days)
print ("Here are the months:", months)

print ("""
There's something going on here.
With the three double-quotes.
We'll be able to type as much as you like.
Even 4 lines if you want, or 5, or 6
"""        
        )