# -*- coding: utf-8 -*-
"""
Created on Wed Feb 22 21:58:27 2017

@author: vriesa
"""

# this one is like your scripts with argv
def print_two(*args):
    arg1, arg2 = args
    print ("arg1: %r, arg2: %r" % (arg1, arg2 ) )
    
# OK, that *args is actually pointless, we can just do this

def print_to_again(arg1,arg2):
        print ("arg1: &r, arg2: %r" % (arg1, arg2) )


def print_one(arg1):
    print ("arg1: %r" % arg1)
    
def print_none():
    print ("I got nothin'")
    
print_two("Zed", "Shaw")
print_two_again("Zed","Shaw")
print_one("First!")
print_none()