name = 'Ale de Vries'
age = 39 # not a lie
height = 72 # inches
weight = 185 # lbs
eyes = 'Brown'
teeth = 'White'
hair = 'Brown'

print ("Let's talk about %s." % name)
print ("He's %d inches tall." % height)
print ("He's %d centimeters tall." % (height * 2.54))
print ("He's %d pounds heavy." % (weight))
print ("He's %d kilos heavy." % (weight * 0.453592 ))
print ("Actually that's not too heavy.")
print ("He's got %s eyes and %s hair." % (eyes.lower(), hair.lower()) )
print ("His teeth are usually %s depending on the coffee" % teeth.lower())

print ("If I add %r, %r, and %r I get %r." % ( age, height, weight, age + height + weight) )