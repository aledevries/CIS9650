# -*- coding: utf-8 -*-
"""
Created on Tue Feb 14 23:43:59 2017

@author: VriesA
"""
from sys import argv

script, filename = argv
#filename = 'test.txt'

print ("Opening the file...")
target = open(filename, 'a')

print ("Do you want to erase the contents of %s? (Y/N)" % filename)
choice = ''
while (choice != 'Y' and choice != 'N'):
    choice = input('> ')

if choice == 'Y':
    print ("Truncating the file.")
    target.truncate(0)

print ("Now I'm going to ask you for three lines.")

line1 = input("line 1:")
line2 = input("line 2:")
line3 = input("line 3:")

print ("I'm going to write these lines to the file.")

target.write(line1)
target.write('\n')
target.write(line2)
target.write('\n')
target.write(line3)
target.write('\n')

print ("And finally, we close it.")
target.close()